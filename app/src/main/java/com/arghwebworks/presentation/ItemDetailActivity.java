package com.arghwebworks.presentation;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.arghwebworks.presentation.api.ApiService;
import com.arghwebworks.presentation.models.APIResponse;
import com.arghwebworks.presentation.models.Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by russ on 5/6/15.
 */
public class ItemDetailActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_detail);
        Intent myintent = getIntent();
        String productid= myintent.getStringExtra( "productid");
        Log.d("zoinks", "onCreate's product id is "+productid);
        final Activity foo = this;


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://cosplay.arghwebworks.com")
                .build();
        ApiService api = restAdapter.create(ApiService.class);

        Callback<APIResponse> callback = new Callback<APIResponse>() {
            @Override
            public void success(APIResponse o, Response response) {

                final Item item = (Item) o.item;
                TextView _bar = (TextView) findViewById(R.id.name);
                _bar.setText( item.name);
                TextView _bag = (TextView) findViewById(R.id.description);
                _bag.setText( item.description);
                TextView _baz = (TextView) findViewById(R.id.price);
                _baz.setText("$" + item.price);

                ImageView _img = (ImageView) findViewById(R.id.image);
                Picasso.with(foo).load(item.bigimage).into(_img);

                //
                Button btn = (Button) findViewById(R.id.shopnow);

                btn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Log.d("zoinks", "heading to "+item.link);

                        Intent intent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(item.link));
                        startActivity(intent);

                    }

                });


            }

            @Override
            public void failure(RetrofitError re){
                Log.d("presentation", re.toString());
            }
        };


        api.productDetails(productid, callback);

    }


}
