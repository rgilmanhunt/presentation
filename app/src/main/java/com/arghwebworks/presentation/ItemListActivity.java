package com.arghwebworks.presentation;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.arghwebworks.presentation.api.ApiService;
import com.arghwebworks.presentation.models.APIResponse;
import com.arghwebworks.presentation.models.Category;
import com.arghwebworks.presentation.models.Item;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by russ on 5/6/15.
 */
public class ItemListActivity extends ListActivity {
    private ItemAdapter itemArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list);
        Intent myintent = getIntent();
        String category = myintent.getStringExtra("category");

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://cosplay.arghwebworks.com")
                .build();
        ApiService api = restAdapter.create(ApiService.class);

        final Activity foo = this;

        Callback<APIResponse> callback = new Callback<APIResponse>() {
            @Override
            public void success(APIResponse o, Response response) {

                List<Item> items = new ArrayList<Item>();

                int max = o.items.size();
                if( max > 10){ max=10; }
                for(int i=0; i< max; i++){
                    Item item = o.items.get(i);
                    items.add(item);
                    Log.d("zoinks", "adding item:"+item.productid);
                }


                itemArrayAdapter = new ItemAdapter(foo,items);
                setListAdapter(itemArrayAdapter);
            }
            @Override
            public void failure(RetrofitError re){
                Log.d("presentation", re.toString() );
            }
        };

        api.productList(category, callback);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        Item item = (Item) l.getItemAtPosition(position);

        Intent intent = new Intent(this, ItemDetailActivity.class);
        intent.putExtra("productid", ""+ item.productid);

        startActivity(intent);
    }
}
