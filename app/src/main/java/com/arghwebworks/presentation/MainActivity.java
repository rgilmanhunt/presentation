package com.arghwebworks.presentation;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.arghwebworks.presentation.api.ApiService;
import com.arghwebworks.presentation.models.APIResponse;
import com.arghwebworks.presentation.models.Category;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ListActivity {
    private ListView stringListView;
    private String[] stringArray;
    private CategoryAdapter catArrayAdapter;
    private List<Category> cats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://cosplay.arghwebworks.com")
                .build();
        ApiService api = restAdapter.create(ApiService.class);

        final Activity foo = this;
        cats = new ArrayList<Category>();
        catArrayAdapter = new CategoryAdapter(foo, cats);
        setListAdapter(catArrayAdapter);


        Callback<APIResponse> callback = new Callback<APIResponse>() {
            @Override
            public void success(APIResponse o, Response response) {
                cats = o.categories;
                catArrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError re) {
                Log.d("presentation", re.toString());
            }
        };

        setContentView(R.layout.activity_main);
        api.categoryList(callback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Category row = (Category) l.getItemAtPosition(position);

        Intent intent = new Intent(this, ItemListActivity.class);
        intent.putExtra("category", row.name);
        startActivity(intent);
    }

}
