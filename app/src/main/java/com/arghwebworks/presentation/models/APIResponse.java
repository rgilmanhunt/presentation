package com.arghwebworks.presentation.models;

import java.util.ArrayList;

/**
 * Created by russ on 5/6/15.
 */
public class APIResponse {
    public int time;
    public ArrayList<Category> categories;
    public ArrayList<Item> items;
    public Item item;

}
