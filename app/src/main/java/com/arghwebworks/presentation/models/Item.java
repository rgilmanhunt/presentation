package com.arghwebworks.presentation.models;

public class Item {
    public String productid;
    public String name;
    public float price;
    public String thumbnail;
    public int id;

    public int merchantid;
    public String merchant;
    public String link;
    public String bigimage;
    public String retailprice;
    public String category;
    public String subcategory;
    public String description;
    public String custom1;
    public String custom2;
    public String custom3;
    public String custom4;
    public String custom5;
    public String lastupdated;
    public String status;
    public String manufacturer;
    public String partnumber;
    public String merchantcategory;
    public String merchantsubcategory;
    public String shortdescription;
    public String isbn;
    public String upc;

}