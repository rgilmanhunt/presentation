package com.arghwebworks.presentation;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.arghwebworks.presentation.models.Item;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * Created by russ on 5/4/15.
 */
public class ItemAdapter extends ArrayAdapter<Item> {
    private LayoutInflater inflater;
    private List<Item> itemholder;
    private Activity activity;


    public ItemAdapter( Activity activity, List<Item> items ){
        super(activity, R.layout.row_item, items);
        inflater = activity.getWindow().getLayoutInflater();
        this.activity=activity;
        itemholder = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final ItemViewHolder holder;
        if (convertView != null) {
            holder = (ItemViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            holder = new ItemViewHolder(convertView);
            convertView.setTag(holder);

        }

        View row = inflater.inflate(R.layout.row_item, parent, false);
        Item i = itemholder.get(position);



        if(i == null ) return row;

        holder.bigTitle.setText( i.name);

        holder.subTitle.setText("$" + i.price);

        holder.text.setText(i.productid);

        Picasso.with(activity).load(i.thumbnail).into(holder.imageView);




        return row;
    }

    static class ItemViewHolder {
        TextView bigTitle;
        TextView subTitle;
        TextView text;
        ImageView imageView;

        ItemViewHolder(View view){
            bigTitle = (TextView) view.findViewById(R.id.bigTitle);
            subTitle=(TextView) view.findViewById(R.id.subTitle);
            text = (TextView) view.findViewById(R.id.text);
            imageView = (ImageView) view.findViewById(R.id.imageView);
        }




        /**
         * http://www.javacodegeeks.com/2013/09/android-viewholder-pattern-example.html
         *
         * butterknife zelezny plugin for generation
         *
         */
    }
}
