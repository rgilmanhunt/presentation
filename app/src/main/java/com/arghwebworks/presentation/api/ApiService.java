package com.arghwebworks.presentation.api;


import com.arghwebworks.presentation.models.APIResponse;
import com.arghwebworks.presentation.models.Category;
import com.arghwebworks.presentation.models.Item;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by russ on 5/6/15.
 */
public interface ApiService {
    @GET("/categories")
    void categoryList(Callback<APIResponse> cb );

    @GET("/categories/{name}")
    void productList(@Path("name") String name, Callback<APIResponse> cb);

    @GET("/product/{product_id}")
    void productDetails(@Path("product_id") String product_id, Callback<APIResponse> cb);
}