package com.arghwebworks.presentation;

/**
 * Created by russ on 5/6/15.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.arghwebworks.presentation.models.Category;

import java.util.List;

/**
 * Created by russ on 5/4/15.
 */
public class CategoryAdapter extends ArrayAdapter<Category> {
    private LayoutInflater inflater;
    private List<Category> catholder;


    public CategoryAdapter( Activity activity, List<Category> cats ){
        super(activity, R.layout.row_category, cats);
        inflater = activity.getWindow().getLayoutInflater();
        catholder=cats;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = inflater.inflate(R.layout.row_category, parent, false);

        TextView _bar = (TextView) row.findViewById(R.id.name);
        _bar.setText( catholder.get(position).name);

        return row;
    }
}